## Training-servyou
### 描述
Training-servyou 是用于运维开发培训项目，用Go语言开发
### 开发环境搭建
1. 安装Go语言（略）

2. 安装pkg-config

    #### macos
        brew install pkg-config
    #### linux
        yum -y install pkg-config

3. 克隆项目

        git clone git@gitlab.com:sttenglish12345/training-servyou.git

4. 设置GOPATH

    ```
    export GOPATH=/PATH_TO_PROJECT/training-servyou
    ```

    

5. 运行项目

         go run servyou.go /PATH_TO_PROJECT/training-servyou/

6. 常用命令

        查看fio所有进程：
        ps -ef | grep servyou
        
        关闭相关进程 ：
        kill -9 进程号
        
        编译应用：
        go build servyou.go
        
        后端启动应用：
        nohup ./servyou /training-servyou/ &         

    
