 package openapi

import (
	//	 "bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"
	//"fmt"
	//	 "encoding/json"
	//	 "net/url"
)

func buildStringToSign(method, urlpath, param string, headers http.Header) (string, string) {
	signatureHeaders := ""
	ss := strings.ToUpper(method) + "\n" //POST
	if v, ok := headers["Accept"]; ok {
		ss += v[0]
	}
	ss += "\n"
	if v, ok := headers["Content-MD5"]; ok {
		ss += v[0]
	}
	ss += "\n"
	if v, ok := headers["Content-Type"]; ok {
		ss += v[0]
	}
	ss += "\n"
	if v, ok := headers["Date"]; ok {
		ss += v[0]
	}
	ss += "\n"
	sorted_hkeys := make([]string, 0)
	for k, _ := range headers {
		if strings.HasPrefix(k, "X-Ca-") {
			sorted_hkeys = append(sorted_hkeys, k)
		}
	}
	sort.Strings(sorted_hkeys)
	for _, k := range sorted_hkeys {
		ss += k + ":" + headers[k][0] + "\n"
		if signatureHeaders != "" {
			signatureHeaders += ","
		}
		signatureHeaders += k
	}
	ss += urlpath

	pp := strings.Split(param, "&")
	params := make(map[string]string)
	sorted_keys := make([]string, 0)

	for _, v := range pp {
		vv := strings.Split(v, "=")
		if len(vv) == 2 {
			sorted_keys = append(sorted_keys, vv[0])
			params[vv[0]] = vv[1]
		}
	}

	sort.Strings(sorted_keys)
	kv := ""
	for _, k := range sorted_keys {
		if kv != "" {
			kv += "&"
		}
		kv += k + "=" + params[k]
	}
	if kv != "" {
		ss += "?" + kv
	}
	return ss, signatureHeaders
}

func computeHMAC_SHA256(message string, secret string) string {
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(message))
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

func IntegrationExecute(api_gate, app_key, secret, api_name string, data string) (string, error) {
	return openApiExecute(api_gate, app_key, secret, "integration", api_name, data)
}

func FiscalExecute(api_gate, app_key, secret, api_name string, data string) (string, error) {
	return openApiExecute(api_gate, app_key, secret, "fiscal", api_name, data)
}

func TestExecute(api_gate, app_key, secret, api_name string, data string) (string, error) {
	return openApiExecute(api_gate, app_key, secret, "test", api_name, data)
}

func SendCarExecute(api_gate, api_name string, data string) (string, error) {
	return sendApiExecute(api_gate, api_name, data)
}

func SendDingDingExecute(api_gate string, data string) (string, error) {
	return postExecute(api_gate, data)
}

func RfsExecute(api_gate, app_key, data string) (string, error) {
	return postRsfExecute(api_gate, app_key, data)
}

func openApiExecute(api_gate, app_key, secret, namespace string, api_name string, data string) (dt string, e error) {
	//SECRET  = "hblhuT0dkFo"
	//APP_KEY = "767748"
	//API_GETWAY = "http://120.55.181.108"
	dt = ""
	urlPath := "/" + namespace + "/" + api_name //PurchasePlanImport"

	var BOM []byte = []byte{'\xEF', '\xBB', '\xBF'} //BOM(Byte Order Mark)
	dd := strings.TrimLeft(string(data), string(BOM))
	methodParam := "_data_=" + dd
	log.Println(api_gate+urlPath, methodParam)

	req, err := http.NewRequest("POST", api_gate+urlPath, strings.NewReader(methodParam))
	if err != nil {
		e = err
	} else {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=utf-8")
		req.Header.Set("X-Ca-Timestamp", strconv.FormatInt(time.Now().Unix(), 10)+"000")
		req.Header.Set("X-Ca-Key", app_key)
		req.Header.Set("X-Ca-Format", "json2")
		stringToSign, signatureHeaders := buildStringToSign("POST", urlPath, methodParam, req.Header)
		req.Header.Set("X-Ca-Signature-Headers", signatureHeaders)
		req.Header.Set("X-Ca-Signature", computeHMAC_SHA256(stringToSign, secret))
		client := &http.Client{}
		//log.Println("resq.body:", req)
		resp, err := client.Do(req)
		if err != nil {
			e = err
			log.Println("err:", err)
		} else {
			bd, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				e = err
				log.Print("err:", err)
			} else {
				dt = string(bd)
				log.Println(dt)
			}
			resp.Body.Close()
		}
	}
	return
}

func sendApiExecute(api_gate string, api_name string, data string) (dt string, e error) {
	dt = ""
	urlPath := "/" + api_name //PurchasePlanImport"

	var BOM []byte = []byte{'\xEF', '\xBB', '\xBF'} //BOM(Byte Order Mark)
	dd := strings.TrimLeft(string(data), string(BOM))
	methodParam := /*"_data_=" +*/ dd
	log.Println(api_gate+urlPath, methodParam)

	req, err := http.NewRequest("POST", api_gate+urlPath /*nil*/, strings.NewReader(methodParam))
	if err != nil {
		e = err
	} else {
		req.Header.Set("Content-Type", "application/json;charset=utf-8")
		client := &http.Client{}
		resp, err := client.Do(req)

		if err != nil {
			e = err
		} else {
			bd, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				e = err
			} else {
				dt = string(bd)
				log.Println(dt)
			}
			resp.Body.Close()
		}
	}
	return
}

func postExecute(api_gate string, data string) (dt string, e error) {
	// https: //oapi.dingtalk.com/robot/send?access_token=9f955c77708d59821005be502a1259b2ec0f3c8b5a4d42b8815a770aaf49424d
	dt = ""

	var BOM []byte = []byte{'\xEF', '\xBB', '\xBF'} //BOM(Byte Order Mark)
	dd := strings.TrimLeft(string(data), string(BOM))
	methodParam := /*"_data_=" +*/ dd
	log.Println(api_gate, methodParam)

	req, err := http.NewRequest("POST", api_gate, strings.NewReader(methodParam))
	if err != nil {
		e = err
	} else {
		req.Header.Set("Content-Type", "application/json;charset=utf-8")
		client := &http.Client{}
		resp, err := client.Do(req)
		log.Println("err:", err)
		if err != nil {
			e = err
		} else {
			bd, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				e = err
			} else {
				dt = string(bd)
				log.Println(dt)
			}
			resp.Body.Close()
		}
	}
	return
}

func postRsfExecute(api_gate, app_key, data string) (dt string, e error) {
	dt = ""
	var BOM []byte = []byte{'\xEF', '\xBB', '\xBF'} //BOM(Byte Order Mark)
	dd := strings.TrimLeft(string(data), string(BOM))
	methodParam := /*"_data_=" +*/ dd
	log.Println(api_gate, methodParam)

	req, err := http.NewRequest("POST", api_gate, strings.NewReader(methodParam))
	if err != nil {
		e = err
	} else {
		req.Header.Set("X-Ca-Key", app_key)
		req.Header.Set("Content-Type", "application/json;charset=utf-8")
		client := &http.Client{}
		resp, err := client.Do(req)
		log.Println("err:", err)
		if err != nil {
			e = err
		} else {
			bd, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				e = err
			} else {
				dt = string(bd)
				log.Println(dt)
			}
			resp.Body.Close()
		}
	}
	return
}
