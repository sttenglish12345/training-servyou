package main

import (
	"github.com/chenjiandongx/go-echarts/charts"
	"log"
	"os"
)

func main() {

	var nameItems = []string{"衬衫", "牛仔裤", "运动裤", "袜子", "冲锋衣", "羊毛衫"}
	bar := charts.NewBar()
	bar.SetGlobalOptions(charts.TitleOpts{Title: "Bar-示例图"})
	bar.AddXAxis(nameItems).
		AddYAxis("商家A", []int{10, 40, 20, 30, 50}).
		AddYAxis("商家B", []int{10, 40, 20, 30, 50})
	f, err := os.Create("./go-echarts/bar.html")
	if err != nil {
		log.Println(err)
	}
	bar.Render(f)
}
