package web

import(
	"html/template"
	"net/http"
	"io/ioutil"
	"net"
	"fmt"
	"time"
	"strings"
	"path/filepath"
)

func RenderHtml( w http.ResponseWriter, dirRun,sitename,templet string, locals map[string]interface{} )( err error ){
	locals["sitename"] = sitename
	var t *template.Template
	head := filepath.Join(dirRun,"/html/header.html")
	foot := filepath.Join(dirRun,"/html/footer.html")
	body := filepath.Join(dirRun,"/html/"+templet+".html")
	t, err = template.ParseFiles( head,body,foot )
	if err==nil {
		err = t.ExecuteTemplate( w,"content",locals )
	}
	return
}

func PageFlip( path string,nrows,pagerows,ipage int) (txt string) {
	nPages := (nrows+pagerows-1) / pagerows
	if ipage<=0 {
		ipage = 1
	}
	if ipage>nPages {
		ipage = nPages
	}
	if nPages>0 {
		if ipage>1 {
			txt += "<a href=\""+path
			if strings.Index(path,"?")>=0 {	txt += "&" } else{ txt += "?" }
			txt += "p=1\" class=\"pageflip\">首页</a>&nbsp;&nbsp;"
			txt += "<a href=\""+path
			if strings.Index(path,"?")>=0 {	txt += "&" } else{ txt += "?" }
			txt += "p="+fmt.Sprintf("%d",ipage-1)+"\" class=\"pageflip\">上页</a>&nbsp;&nbsp;"
		}
		
		s:=ipage-4;
		if s<1 { s = 1 }
		e:=s+9
		if e>nPages { e = nPages }
		if e-s<9 {
			s = e-9
			if s<1 { s = 1 }
		}
		for i:=s;i<=e;i++ {
			if i==ipage {
				txt += "<span class=\"pagecursor\">"+fmt.Sprintf("%d",i)+"</span>&nbsp;"
			}else{
				txt += "<a href=\""+path
				if strings.Index(path,"?")>=0 {	txt += "&" } else{ txt += "?" }
				txt += "p="+fmt.Sprintf("%d",i)+"\" class=\"pageflip\">"+fmt.Sprintf("%d",i)+"</a>&nbsp;"
			}
		}

		if ipage<nPages {
			txt += "<a href=\""+path
			if strings.Index(path,"?")>=0 {	txt += "&" } else{ txt += "?" }
			txt += "p="+fmt.Sprintf("%d",ipage+1)+"\" class=\"pageflip\">下页</a>&nbsp;&nbsp;"
			txt += "<a href=\""+path
			if strings.Index(path,"?")>=0 {	txt += "&" } else{ txt += "?" }
			txt += "p="+fmt.Sprintf("%d",nPages)+"\" class=\"pageflip\">尾页</a>"
		}
	}
	return
}

var timeConnectLimit time.Duration = 30
var timeGetHeadLimit time.Duration = 30
var timeGetPageLimit time.Duration = 120
func DoRequest(url string)(ret string,derr error){
    transport := &http.Transport{
		Dial: func(network, addr string) (net.Conn, error) {
			conn, err := net.DialTimeout(network, addr, timeConnectLimit*time.Second)//seconds connect limit
			if err != nil {
				return nil,err
			}
			deadline := time.Now().Add(timeGetHeadLimit*time.Second)//seconds get head limit
			e := conn.SetDeadline(deadline)
			if e!=nil {
				return nil,e
			}
			return conn, nil
		},
		ResponseHeaderTimeout: timeGetHeadLimit*time.Second,
		DisableKeepAlives: true,
	}
	client := &http.Client{Transport: transport,Timeout: timeGetPageLimit*time.Second }
	
	req, err := http.NewRequest("POST", url, nil)
    if err != nil {
        derr = err
		return
    }
 
   req.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")

    resp, err := client.Do(req)
	
	if err != nil {
		derr = err
	}else{
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			derr = err
		}
		ret = string( body )
	}
	return	
}
