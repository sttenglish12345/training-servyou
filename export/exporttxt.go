package main

import (
	"os"
)

func main() {
	filename := "./export/test.txt"
	var content string
	f, err := os.Create(filename)
	defer f.Close()
	if err != nil {
		// 创建文件失败处理

	} else {
		content = "需要写入的文件内容"
		_, err = f.Write([]byte(content))
		if err != nil {
			// 写入失败处理

		}
	}
}
