package main

import (
	"github.com/360EntSecGroup-Skylar/excelize"
)

func main(){
	xlsx := excelize.NewFile()
	categories := map[string]string{"A1": "Queryid", "B1": "Query", "C1": "Elapsedtime", "D1": "Inputdatasize", "E1": "Peekmemorysize", "F1": "Createtime"}
	for k, v := range categories {
		xlsx.SetCellValue("Sheet1", k, v)
	}
	xlsx.SetCellValue("sheet1", "A2", "test")

	err := xlsx.SaveAs("./export/test.xlsx")
	if err != nil {
		panic(err)
	}
}
