package main

import (
	"bufio"
	"database/sql"
	"fio-new/crawlercorps.com/config"
	"github.com/gorilla/context"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"servyoutraing/monitor"
	"strings"
	_ "github.com/Go-SQL-Driver/mysql"
)

var dirRun string
var openSignup bool
var mysql_host, mysql_port, mysql_database, mysql_user, mysql_pswd string
var domain, sitename, language, fromemail, smtpserver, smtpport, smtppswd, monitoremail, monitordingding string
var API_GATEWAY, APP_KEY, SECRET, SESSION_KEY string
var users string
var mapHint map[string]string

func getRunDir() string {
	var dir string
	ostype := runtime.GOOS
	if ostype == "darwin" {
		dir, _ = filepath.Abs(filepath.Dir(os.Args[1]))
	} else {
		dir, _ = filepath.Abs(filepath.Dir(os.Args[0]))
	}
	log.Println("dir###:", dir)
	//dir, _ := filepath.Abs(filepath.Dir(os.Args[1]))
	//if strings.Index(dir, "go-build") > 0 {
	//	if runtime.GOOS == "linux" {
	//		dir = "/dat/crawler/servyou"
	//	} else {
	//		dir = "d:\\crawler\\servyou"
	//	}
	//}
	return dir
}

func setLogger(dirRun string) *os.File {
	logfile, err := os.OpenFile(filepath.Join(dirRun, "servyou.log"), os.O_WRONLY /*| os.O_APPEND*/ |os.O_CREATE /*os.ModeAppend | */, 0666)

	if err == nil {
		log.SetOutput(logfile)
		log.SetFlags(log.Flags() | log.Lshortfile)
		log.Println("servyou start ...")
	}
	return logfile
}

func isExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	return os.IsExist(err)
}

func readParam() {
	openSignup = false
	domain, sitename, language = "", "", "en"
	mysql_host, mysql_port, mysql_database, mysql_user, mysql_pswd = "", "", "", "", ""
	monitoremail, monitordingding = "", ""
	cfile := filepath.Join(dirRun, "servyou.conf")
	if isExists(cfile) {
		params := config.LoadConfigure(cfile)
		if params != nil {

			domain = params["domain"]
			sitename = params["sitename"]
			language = params["language"]

			fromemail = params["fromemail"]
			smtpserver = params["smtpserver"]
			smtpport = params["smtpport"]
			smtppswd = params["smtppswd"]

			monitoremail = params["monitoremail"]
			monitordingding = params["monitordingding"]

			API_GATEWAY = params["API_GATEWAY"]
			APP_KEY = params["APP_KEY"]
			SECRET = params["SECRET"]
			SESSION_KEY = params["SESSION_KEY"]

			mysql_host = params["mysql_host"]
			mysql_port = params["mysql_port"]
			mysql_database = params["mysql_database"]
			mysql_user = params["mysql_user"]
			mysql_pswd = params["mysql_pswd"]

			users = params["users"]

			signup := params["signup"]
			if signup == "open" {
				openSignup = true
			}
		}
	}
	if len(mysql_user) == 0 {
		log.Println(mapHint["NO_MYSQL_PARAM"])
		os.Exit(1)
	}
}

func readHint() {
	mapHint = make(map[string]string)
	fname := filepath.Join(dirRun, "static")
	fname = filepath.Join(fname, language+".lang")
	fi, err := os.Open(fname)
	if err == nil {
		defer fi.Close()
		eof := false
		r := bufio.NewReader(fi)
		for {
			s, e := r.ReadString('\n')
			if e == io.EOF {
				eof = true
			}

			if len(s) > 0 {
				ss := strings.Split(strings.TrimRight(s, "\r\n"), "=")
				if len(ss) == 2 {
					k := strings.TrimRight(ss[0], " \t")
					v := strings.TrimLeft(ss[1], " \t")
					mapHint[k] = v
				}
			}
			if eof {
				break
			}
		}
	}
}

func staticDirHandler(mux *http.ServeMux, prefix string, staticDir string) { //serve all static file: image,css,js
	mux.HandleFunc(prefix,
		func(w http.ResponseWriter, r *http.Request) {
			file := staticDir + r.URL.Path[len(prefix)-1:]
			if isExists(file) {
				http.ServeFile(w, r, file)
			} else {
				http.NotFound(w, r)
				return
			}
		})
}

func trainingHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Training(dirRun, sitename, w, r)

}

func primaryHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Primary(dirRun, sitename, w, r)

}

func dingdingaiHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Dingdingai(dirRun, sitename, w, r)

}

func introductionHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Introduction(dirRun, sitename, w, r)

}

func characteristicHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Characteristic(dirRun, sitename, w, r)

}

func productsHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Products(dirRun, sitename, w, r)

}

func compareLanguageHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Comparelanguage(dirRun, sitename, w, r)

}

func standardlibraryHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Standardlibrary(dirRun, sitename, w, r)

}

func grammarHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Grammar(dirRun, sitename, w, r)

}

func studyHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Study(dirRun, sitename, w, r)

}

func environmentHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Environment(dirRun, sitename, w, r)

}

func structureHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Structure(dirRun, sitename, w, r)

}

func commandgoHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Commandgo(dirRun, sitename, w, r)

}

func idetoolHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Idetool(dirRun, sitename, w, r)

}

func environmentsummaryHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Environmentsummary(dirRun, sitename, w, r)

}

func basegoHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Basego(dirRun, sitename, w, r)

}

func analyseHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Analyse(dirRun, sitename, w, r)

}

func exportHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Export(dirRun, sitename, w, r)

}

func databaseHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Database(dirRun, sitename, w, r)

}

func reorganizeHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Reorganize(dirRun, sitename, w, r)

}

func debuggoHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Debuggo(dirRun, sitename, w, r)

}

func loggoHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Loggo(dirRun, sitename, w, r)

}

func basesummaryHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Basesummary(dirRun, sitename, w, r)

}

func packagegoHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Packagego(dirRun, sitename, w, r)

}

func intermediateHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Intermediate(dirRun, sitename, w, r)

}

func webgoHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Webgo(dirRun, sitename, w, r)

}

func confHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Conf(dirRun, sitename, w, r)

}

func exporttxttHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Exporttxtt(dirRun, sitename, w, r)

}

func exporttexetHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Exporttexet(dirRun, sitename, w, r)

}

func accessdbHandler(w http.ResponseWriter, r *http.Request) {

		monitor.Accessdb(dirRun, sitename, w, r)

}

func dbviewHandler(w http.ResponseWriter, r *http.Request) {

	db, oerr := openMySQLDB()
	if oerr == nil {
		monitor.Dbview(db, dirRun, sitename, w, r)
	} else {
		log.Println(oerr)
	}
	defer db.Close()

}

func ossHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Oss(dirRun, sitename, w, r)

}

func base64Handler(w http.ResponseWriter, r *http.Request) {

	monitor.Base64(dirRun, sitename, w, r)

}

func rpcHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Rpc(dirRun, sitename, w, r)

}

func restfulHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Restful(dirRun, sitename, w, r)

}

func javaapiHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Javaapi(dirRun, sitename, w, r)

}

func openapiHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Openapi(dirRun, sitename, w, r)

}

func aliyunHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Aliyun(dirRun, sitename, w, r)

}

func itsspiderHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Itsspider(dirRun, sitename, w, r)

}

func scrapyHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Scrapy(dirRun, sitename, w, r)

}

func spiderpicHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Spiderpic(dirRun, sitename, w, r)

}

func emailHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Email(dirRun, sitename, w, r)

}

func browserHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Browser(dirRun, sitename, w, r)

}

func goechartsHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Goecharts(dirRun, sitename, w, r)

}

func gitreadmeHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Gitreadme(dirRun, sitename, w, r)

}

func gitHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Git(dirRun, sitename, w, r)

}

func highHandler(w http.ResponseWriter, r *http.Request) {

	monitor.High(dirRun, sitename, w, r)

}
func concurrentHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Concurrent(dirRun, sitename, w, r)

}

func distributedHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Distributed(dirRun, sitename, w, r)

}

func smallserviceHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Smallservice(dirRun, sitename, w, r)

}

func chanelgoHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Chanelgo(dirRun, sitename, w, r)

}

func k8sHandler(w http.ResponseWriter, r *http.Request) {

	monitor.K8s(dirRun, sitename, w, r)

}

func dockerapidHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Dockerapid(dirRun, sitename, w, r)

}

func upgradeHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Upgrade(dirRun, sitename, w, r)

}

func devopsHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Devops(dirRun, sitename, w, r)

}

func gameHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Game(dirRun, sitename, w, r)

}

func communityHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Community(dirRun, sitename, w, r)

}

func openMySQLDB() (db *sql.DB, err error) {
	db, err = sql.Open("mysql", mysql_user+":"+mysql_pswd+"@tcp("+mysql_host+":"+mysql_port+")/"+mysql_database+"?charset=utf8")
	if err == nil {
		err = db.Ping()
		if err == nil {
			log.Println("mysql DB Ping Success!")
		} else {
			log.Println("mysql connect ", err)
		}
	} else {
		log.Println("mysql Connect:", err)
	}
	return
}

func main() {
	log.Println("servyoutraining starting......")
	dirRun = getRunDir() //get director
	logfile := setLogger(dirRun)
	defer logfile.Close()
	readParam()
	readHint()

	os.Setenv("NLS_LANG", "AMERICAN_AMERICA.ZHS16GBK")
	var err error

	if err == nil {

		mux := http.NewServeMux()
		staticDirHandler(mux, "/t/", "./static")

		//training
		mux.HandleFunc("/servyou", trainingHandler)

		//primary
		mux.HandleFunc("/primary", primaryHandler)
		mux.HandleFunc("/introduction", introductionHandler)
		mux.HandleFunc("/characteristic", characteristicHandler)
		mux.HandleFunc("/products", productsHandler)
		mux.HandleFunc("/comparelanguage", compareLanguageHandler)
		mux.HandleFunc("/standardlibrary", standardlibraryHandler)
		mux.HandleFunc("/grammar", grammarHandler)
		mux.HandleFunc("/study", studyHandler)
		mux.HandleFunc("/environment", environmentHandler)
		mux.HandleFunc("/structure", structureHandler)
		mux.HandleFunc("/commandgo", commandgoHandler)
		mux.HandleFunc("/idetool", idetoolHandler)
		mux.HandleFunc("/environmentsummary", environmentsummaryHandler)
		mux.HandleFunc("/basego", basegoHandler)
		mux.HandleFunc("/analyse", analyseHandler)
		mux.HandleFunc("/export", exportHandler)
		mux.HandleFunc("/database", databaseHandler)
		mux.HandleFunc("/reorganize", reorganizeHandler)
		mux.HandleFunc("/debuggo", debuggoHandler)
		mux.HandleFunc("/loggo", loggoHandler)
		mux.HandleFunc("/basesummary", basesummaryHandler)
		mux.HandleFunc("/packagego", packagegoHandler)
		//intermediate
		mux.HandleFunc("/intermediate", intermediateHandler)
		mux.HandleFunc("/webgo", webgoHandler)
		mux.HandleFunc("/getconf", confHandler)
		mux.HandleFunc("/exporttxt", exporttxttHandler)
		mux.HandleFunc("/exportexe", exporttexetHandler)
		mux.HandleFunc("/accessdb", accessdbHandler)
		mux.HandleFunc("/dbview", dbviewHandler)
		mux.HandleFunc("/oss", ossHandler)
		mux.HandleFunc("/base64", base64Handler)
		mux.HandleFunc("/rpc", rpcHandler)
		mux.HandleFunc("/restful", restfulHandler)
		mux.HandleFunc("/javaapi", javaapiHandler)
		mux.HandleFunc("/openapi", openapiHandler)
		mux.HandleFunc("/aliyun", aliyunHandler)
		mux.HandleFunc("/itsspider", itsspiderHandler)
		mux.HandleFunc("/scrapy", scrapyHandler)
		mux.HandleFunc("/dingdingai", dingdingaiHandler)
		mux.HandleFunc("/email", emailHandler)
		mux.HandleFunc("/spiderpic", spiderpicHandler)
		mux.HandleFunc("/browser", browserHandler)
		mux.HandleFunc("/goecharts", goechartsHandler)
		mux.HandleFunc("/gitreadme", gitreadmeHandler)
		mux.HandleFunc("/git", gitHandler)
		//high
		mux.HandleFunc("/high", highHandler)
		mux.HandleFunc("/concurrent", concurrentHandler)
		mux.HandleFunc("/distributed", distributedHandler)
		mux.HandleFunc("/smallservice", smallserviceHandler)
		mux.HandleFunc("/chanelgo", chanelgoHandler)
		mux.HandleFunc("/k8s", k8sHandler)
		mux.HandleFunc("/dockerapid", dockerapidHandler)
		mux.HandleFunc("/upgrade", upgradeHandler)
		mux.HandleFunc("/devops", devopsHandler)
		mux.HandleFunc("/game", gameHandler)
		mux.HandleFunc("/community", communityHandler)

		err = http.ListenAndServe(":7777", context.ClearHandler(mux))
		if err != nil {
			log.Fatal("ListenAndServe", err.Error())
		}
	}
}
