package main

import (
	"bufio"
	"fio-new/crawlercorps.com/config"
	"github.com/gorilla/context"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"servyoutraing/monitor"
)

var dirRun string
var openSignup bool
var mysql_host, mysql_port, mysql_database, mysql_user, mysql_pswd string
var domain, sitename, language, fromemail, smtpserver, smtpport, smtppswd, monitoremail, monitordingding string
var API_GATEWAY, APP_KEY, SECRET, SESSION_KEY string
var ora_host, ora_port, ora_sid, ora_user, ora_pswd string
var users string
var mapHint map[string]string

func getRunDir() string {
	var dir string
	ostype:=runtime.GOOS
	if ostype=="darwin"{
		dir, _ = filepath.Abs(filepath.Dir(os.Args[1]))
	}else {
		dir, _ = filepath.Abs(filepath.Dir(os.Args[0]))
	}
	log.Println("dir###:",dir)

	return dir
}

func setLogger(dirRun string) *os.File {
	logfile, err := os.OpenFile(filepath.Join(dirRun, "itsspider.log"), os.O_WRONLY /*| os.O_APPEND*/ |os.O_CREATE /*os.ModeAppend | */, 0666)

	if err == nil {
		log.SetOutput(logfile)
		log.SetFlags(log.Flags() | log.Lshortfile)
		log.Println("servyou start ...")
	}
	return logfile
}

func isExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	return os.IsExist(err)
}

func readParam() {
	openSignup = false
	domain, sitename, language = "", "", "en"
	mysql_host, mysql_port, mysql_database, mysql_user, mysql_pswd = "", "", "", "", ""
	monitoremail, monitordingding = "", ""
	cfile := filepath.Join(dirRun, "servyou.conf")
	if isExists(cfile) {
		params := config.LoadConfigure(cfile)
		if params != nil {

			domain = params["domain"]
			sitename = params["sitename"]
			language = params["language"]

			fromemail = params["fromemail"]
			smtpserver = params["smtpserver"]
			smtpport = params["smtpport"]
			smtppswd = params["smtppswd"]

			monitoremail = params["monitoremail"]
			monitordingding = params["monitordingding"]

			API_GATEWAY = params["API_GATEWAY"]
			APP_KEY = params["APP_KEY"]
			SECRET = params["SECRET"]
			SESSION_KEY = params["SESSION_KEY"]

			mysql_host = params["mysql_host"]
			mysql_port = params["mysql_port"]
			mysql_database = params["mysql_database"]
			mysql_user = params["mysql_user"]
			mysql_pswd = params["mysql_pswd"]

			ora_host = params["ora_host"]
			ora_port = params["ora_port"]
			ora_sid = params["ora_sid"]
			ora_user = params["ora_user"]
			ora_pswd = params["ora_pswd"]

			users = params["users"]

			signup := params["signup"]
			if signup == "open" {
				openSignup = true
			}
		}
	}
	if len(mysql_user) == 0 {
		log.Println(mapHint["NO_MYSQL_PARAM"])
		os.Exit(1)
	}
}

func readHint() {
	mapHint = make(map[string]string)
	fname := filepath.Join(dirRun, "static")
	fname = filepath.Join(fname, language+".lang")
	fi, err := os.Open(fname)
	if err == nil {
		defer fi.Close()
		eof := false
		r := bufio.NewReader(fi)
		for {
			s, e := r.ReadString('\n')
			if e == io.EOF {
				eof = true
			}

			if len(s) > 0 {
				ss := strings.Split(strings.TrimRight(s, "\r\n"), "=")
				if len(ss) == 2 {
					k := strings.TrimRight(ss[0], " \t")
					v := strings.TrimLeft(ss[1], " \t")
					mapHint[k] = v
				}
			}
			if eof {
				break
			}
		}
	}
}

func staticDirHandler(mux *http.ServeMux, prefix string, staticDir string) { //serve all static file: image,css,js
	mux.HandleFunc(prefix,
		func(w http.ResponseWriter, r *http.Request) {
			file := staticDir + r.URL.Path[len(prefix)-1:]
			if isExists(file) {
				http.ServeFile(w, r, file)
			} else {
				http.NotFound(w, r)
				return
			}
		})
}

func trainingHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Training(dirRun, sitename, w, r)

}

func dingdingrobotHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Dingdingrobot(users,dirRun, sitename, w, r)

}

func introductionHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Introduction(dirRun, sitename, w, r)

}

func itsspiderHandler(w http.ResponseWriter, r *http.Request) {

	monitor.Itsspider(dirRun, sitename, w, r)

}

func main() {
	log.Println("servyoutraining starting......")
	dirRun = getRunDir() //get director
	logfile := setLogger(dirRun)
	defer logfile.Close()
	//readParam()
	readHint()

	os.Setenv("NLS_LANG", "AMERICAN_AMERICA.ZHS16GBK")
	var err error

	if err == nil {

		mux := http.NewServeMux()
		staticDirHandler(mux, "/t/", "./static")

		//training
		mux.HandleFunc("/itsspider", itsspiderHandler)

		err = http.ListenAndServe(":7777", context.ClearHandler(mux))
		if err != nil {
			log.Fatal("ListenAndServe", err.Error())
		}
	}
}
