package main

import (
	"fmt"
	"runtime"
	"strings"
)

func main() {

	str := " http://www.11.com" + "\r\n" +
		"htttp://www.22.com" + "\r\n" +
		"http://www.33.com"

	for k, v := range strings.Split(str, "\r\n") {
		fmt.Println(k)
		fmt.Println(v)
	}

	//fmt.Println(os.Args)
	fmt.Println(runtime.GOOS)

	fmt.Println(strings.HasSuffix(str, "m"))

	var x float64 = 8.8
	var y int = int(x)
	fmt.Println(y) // outputs "5"
	fmt.Println("--------") // outputs "5"

	var vi,vj int
	endfor := false
	for i := 1; i <= 10; i++ {
		vj=i
		endfor=true

		for j := 5; j < i; j-- {
			if j == 3 {
				vi = j
				endfor=true
				break
			}
		}
	}
	fmt.Println("vi:", vi,"##vj",vj)
}
