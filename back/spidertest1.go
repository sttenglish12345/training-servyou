package main

import (
	"io/ioutil"
	"net/http"
	"os"
)

/**
* 反爬 Cookie, Referer等验证
* 返回response
*/
func getResponse2(url string) *http.Response {
	client := &http.Client{}
	request, _ := http.NewRequest("GET", url, nil)
	request.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0")
	response, _ := client.Do(request)
	return response
}

//定义新的数据类型
type Spider struct {
	url    string
	header map[string]string
}

//定义 Spider get的方法
func (keyword Spider) get_html_header() string {
	client := &http.Client{}
	req, err := http.NewRequest("GET", keyword.url, nil)
	if err != nil {
	}
	for key, value := range keyword.header {
		req.Header.Add(key, value)
	}
	resp, err := client.Do(req)
	if err != nil {
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
	}
	return string(body)

}

func parse3() {
	header := map[string]string{
		"Host":                      "movie.douban.com",
		"Connection":                "keep-alive",
		"Cache-Control":             "max-age=0",
		"Upgrade-Insecure-Requests": "1",
		"User-Agent":                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36",
		"Accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Referer":                   "https://movie.douban.com/top250",
	}

	//create file
	f, err := os.Create("./parse03.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	//写入标题
	f.WriteString("文章标题" + "\r\n")

	//循环每页解析并把结果写入excel
	url := "https://blog.csdn.net/bysjlwdx"
	spider := &Spider{url, header}
	html := spider.get_html_header()

	f.WriteString(html + "\r\n"+ "\r\n"+ "\r\n")
}

func main() {

	//url := "https://blog.csdn.net/bysjlwdx"
	//resp := getResponse2(url)
	////
	//defer resp.Body.Close()
	//
	//html := resp.Body
	////strings.NewReader(html)
	//
	//dom, err := goquery.NewDocumentFromReader(html)
	//if err != nil {
	//	log.Fatalln("失败原因", err)
	//}
	//
	//fmt.Println(dom.Find("title").Text())

	parse3()

}
