package monitor

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/PuerkitoBio/goquery"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"servyoutraing/open"
	"servyoutraing/web"
	"strconv"
	"strings"
	"time"
)

func Training(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})

	web.RenderHtml(w, dirRun, sitename, "home", locals)
}

func Primary(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})

	web.RenderHtml(w, dirRun, sitename, "primary/primary", locals)
}

func Introduction(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}

	web.RenderHtml(w, dirRun, sitename, "primary/introduction", locals)
}

func Characteristic(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/characteristic", locals)
}

func Products(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/products", locals)
}

func Comparelanguage(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/comparelanguage", locals)
}

func Standardlibrary(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/standardlibrary", locals)
}

func Grammar(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/grammar", locals)
}

func Study(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/study", locals)
}

func Environment(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/environment", locals)
}

func Structure(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/structure", locals)
}

func Commandgo(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/commandgo", locals)
}

func Idetool(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/idetool", locals)
}

func Environmentsummary(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/environmentsummary", locals)
}

func Basego(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/basego", locals)
}

func Analyse(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/analyse", locals)
}

func Export(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/export", locals)
}

func Database(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/database", locals)
}

func Reorganize(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/reorganize", locals)
}

func Debuggo(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/debuggo", locals)
}

func Loggo(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/loggo", locals)
}

func Basesummary(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/basesummary", locals)
}

func Packagego(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "primary/packagego", locals)
}

func Intermediate(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/intermediate", locals)
}

func Webgo(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/webgo", locals)
}

func Conf(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}

	var areainput string
	urlfile := filepath.Join(dirRun, "itsspider.conf")
	b, err := ioutil.ReadFile(urlfile)
	if err == nil {
		areainput = strings.TrimRight(string(b), "\r\n")
	}
	locals["areainput"] = areainput
	web.RenderHtml(w, dirRun, sitename, "intermediate/conf", locals)
}

func Exporttxtt(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/exporttxtt", locals)
}

func Exporttexet(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/exporttexet", locals)
}

func Accessdb(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}

	web.RenderHtml(w, dirRun, sitename, "intermediate/accessdb", locals)
}

func Dbview(db *sql.DB, dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}

	type dbview struct {
		Bg            string
		Email_address string
		User_id       int
	}

	dbviews := []dbview{}

	asql := "select user_id,email_address from user"
	rows, err := db.Query(asql)
	if err == nil {
		n := 0
		for rows.Next() {
			var email_address sql.NullString
			var user_id int
			e := rows.Scan(&user_id, &email_address)
			if e == nil {
				var d dbview
				n ++
				if n%2 == 0 {
					d.Bg = "F3F3F3"
				} else {
					d.Bg = "FFFFFF"
				}
				d.User_id = user_id
				d.Email_address = email_address.String

				dbviews = append(dbviews, d)
			} else {
				log.Println(e)
			}
		}
		rows.Close()
	} else {
		log.Println(err)
	}
	locals["dbviews"] = dbviews
	web.RenderHtml(w, dirRun, sitename, "intermediate/dbview", locals)
}

func Oss(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/oss", locals)
}

func Base64(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/base64", locals)
}

func Rpc(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/rpc", locals)
}

func Restful(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/restful", locals)
}

func Javaapi(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/javaapi", locals)
}

func Openapi(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/openapi", locals)
}

func Aliyun(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/aliyun", locals)
}

//func Itsspider(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
//	locals := make(map[string]interface{})
//	t := r.FormValue("t")
//	p := r.FormValue("p")
//	if t == "0" {
//		locals["t"] = 0
//	}else if  t == "1" {
//		locals["t"] = 1
//		locals["p"] = p
//	}
//	web.RenderHtml(w, dirRun, sitename, "intermediate/itsspider", locals)
//}

func Scrapy(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/scrapy", locals)
}

func Email(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/email", locals)
}
func Spiderpic(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/spiderpic", locals)
}

func Browser(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/browser", locals)
}

func randInt(nameItems []string) []int {
	//var nameItems = []string{"衬衫", "牛仔裤", "运动裤", "袜子", "冲锋衣", "羊毛衫"}
	var seed = rand.NewSource(time.Now().UnixNano())
	cnt := len(nameItems)
	r := make([]int, 0)
	for i := 0; i < cnt; i++ {
		r = append(r, int(seed.Int63())%50)
	}
	return r
}

func Goecharts(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}

	//var nameItems = []string{"衬衫", "牛仔裤", "运动裤", "袜子", "冲锋衣", "羊毛衫"}
	//
	//bar := charts.NewBar()
	//bar.SetGlobalOptions(charts.TitleOpts{Title: "Bar-示例图"}, charts.ToolboxOpts{Show: true})
	//bar.AddXAxis(nameItems).
	//	AddYAxis("商家A", randInt(nameItems)).
	//	AddYAxis("商家B", randInt(nameItems))
	//f, err := os.Create("./html/intermediate/bar.html")
	//if err != nil {
	//	log.Println(err)
	//}
	//bar.Render(f)

	web.RenderHtml(w, dirRun, sitename, "intermediate/goecharts", locals)
}

func Gitreadme(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/gitreadme", locals)
}

func Git(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/git", locals)
}

func High(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})

	web.RenderHtml(w, dirRun, sitename, "high/high", locals)
}

func Concurrent(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "high/concurrent", locals)
}

func Distributed(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "high/distributed", locals)
}

func Smallservice(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "high/smallservice", locals)
}

func Chanelgo(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "high/chanelgo", locals)
}

func K8s(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "high/k8s", locals)
}

func Dockerapid(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "high/dockerapid", locals)
}

func Upgrade(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "high/upgrade", locals)
}

func Devops(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "intermediate/devops", locals)
}

func Game(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "high/game", locals)
}

func Community(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "high/community", locals)
}

/**
* 反爬 Cookie, Referer等验证
* 返回response
*/
func getResponse(url string) (*http.Response, error) {
	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)
	request.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0")
	response, _ := client.Do(request)
	return response, err
}

type mesgT struct {
	Bg             string
	Queryid        string
	Query          string
	Elapsedtime    string
	Inputdatasize  string
	Peekmemorysize string
	Createtime     string
}

func Itsspider(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	log.Println("Itsspider start...")
	locals := make(map[string]interface{})

	//catch exception
	defer func() {
		if err := recover(); err != nil {
			locals["mesg"] = "爬取成功！"
			log.Println("****itsspider crawling err****:", err)
		}
	}()

	oversize := strings.Trim(r.FormValue("oversize"), "\r\n\t")
	if oversize == "" {
		oversize = "100"
	}
	overtime := strings.Trim(r.FormValue("overtime"), "\r\n\t")
	if overtime == "" {
		overtime = "60"
	}
	areainput := strings.Trim(r.FormValue("area_input"), "\r\n\t")
	confsave := strings.Trim(r.FormValue("confsave"), "\r\n\t")
	btexport := strings.Trim(r.FormValue("btexport"), "\r\n\t")
	log.Println("oversize:", oversize)
	log.Println("overtime:", overtime)
	log.Println("url:", areainput)

	url := strings.Split(areainput, "\r\n")
	sizeset, _ := strconv.Atoi(oversize)
	timeset, _ := strconv.Atoi(overtime)
	var errout error
	var m mesgT
	mesg := []mesgT{}
	records := 0
	lessrecods := 0
	//to excel
	xlsx := excelize.NewFile()
	categories := map[string]string{"A1": "Queryid", "B1": "Query", "C1": "Elapsedtime", "D1": "Inputdatasize", "E1": "Peekmemorysize", "F1": "Createtime"}
	for k, v := range categories {
		xlsx.SetCellValue("Sheet1", k, v)
	}
	n := 2
	if r.Method == "POST" {
		for _, v := range url {
			resp, err := getResponse(v)
			if err != nil {
				errout = err
				log.Fatalln("失败原因:", err)
			}

			//html := resp.Body
			//strings.NewReader(resp.Body)
			if resp != nil {
				if resp.StatusCode == 200 {
					defer resp.Body.Close()
					dom, err := goquery.NewDocumentFromReader(resp.Body)
					if err != nil {
						errout = err
						log.Fatalln("失败原因:", err)
					}

					//fmt.Println(dom.Find("title").Text())
					//locals["area_output"] = dom.Find("title").Text()
					//m.Queryid = dom.Find("title").Text() + "\r\n"
					//mesg = append(mesg, m)
					dom.Find("tbody tr").Each(func(i int, s *goquery.Selection) {
						//fmt.Println(s.Find("td:nth-child(1)").Text(),"##",s.Find("td:nth-child(4)").Text())

						elapsedtime := s.Find("td:nth-child(3)").Text()
						var et2s int
						if strings.HasSuffix(elapsedtime, "m") {
							tmp := strings.Replace(elapsedtime, "m", "", -1)
							if strings.Contains(tmp, ".") {
								f1, _ := strconv.ParseFloat(tmp, 64)
								fmt.Sprintf("%.2f", f1)
								et2s = int(f1 * 60)
							} else {
								i1, _ := strconv.Atoi(tmp)
								et2s = i1 * 60
							}
						} else if strings.HasSuffix(elapsedtime, "s") && !strings.HasSuffix(elapsedtime, "ms") {
							etsplit := strings.Replace(elapsedtime, "s", "", -1)
							vv := strings.Split(etsplit, ".")
							if len(vv) == 2 {
								etsplit = vv[0]
							}
							et2s, _ = strconv.Atoi(etsplit)
						}

						memorysize := s.Find("td:nth-child(5)").Text()
						if strings.HasSuffix(memorysize, "GB") && et2s > 0 {
							mssplit := strings.Replace(memorysize, "GB", "", -1)
							v := strings.Split(mssplit, ".")
							if len(v) == 2 {
								mssplit = v[0]
							}
							ms, _ := strconv.Atoi(mssplit)

							if ms >= sizeset && et2s >= timeset {
								log.Println("oversize:", oversize)
								log.Println("Queryid:", s.Find("td:nth-child(1)").Text())
								if n%2 == 0 {
									m.Bg = "F3F3F3"
								} else {
									m.Bg = "FFFFFF"
								}
								m.Queryid = s.Find("td:nth-child(1)").Text()
								m.Query = s.Find("td:nth-child(2)").Text()
								m.Elapsedtime = elapsedtime
								m.Inputdatasize = s.Find("td:nth-child(4)").Text()
								m.Peekmemorysize = memorysize
								m.Createtime = s.Find("td:nth-child(6)").Text() + "\r\n"
								mesg = append(mesg, m)
								records++

								//to excel save
								xlsx.SetCellValue("sheet1", "A"+strconv.Itoa(n), m.Queryid)
								xlsx.SetCellValue("sheet1", "B"+strconv.Itoa(n), m.Query)
								xlsx.SetCellValue("sheet1", "C"+strconv.Itoa(n), m.Elapsedtime)
								xlsx.SetCellValue("sheet1", "D"+strconv.Itoa(n), m.Inputdatasize)
								xlsx.SetCellValue("sheet1", "E"+strconv.Itoa(n), m.Peekmemorysize)
								xlsx.SetCellValue("sheet1", "F"+strconv.Itoa(n), m.Createtime)
								n = n + 1

							} else {
								lessrecods++
							}
							records++
						}
					})
				} else if resp.StatusCode == 403 {
					errout = errors.New("IP 已被禁止访问.url path is:  " + v)
					break
				} else {
					errout = errors.New("Failed：" + resp.Status + ".url path is:  " + v)
					break
				}
			} else {
				errout = errors.New("Failed 爬取的不是有效地址.url path is:  " + v)
				break
			}
		}
		if errout == nil {
			locals["mesg"] = "Success！爬取成功，内容如下： "
			if records == lessrecods {
				locals["mesg"] = "Success！爬取成功，内容如下： No records."
			}
		} else {
			locals["mesg"] = errout.Error()
		}
	} else {
		urlfile := filepath.Join(dirRun, "itsspider.conf")
		b, err := ioutil.ReadFile(urlfile)
		if err == nil {
			areainput = strings.TrimRight(string(b), "\r\n")
		}
	}

	locals["oversize"] = oversize
	locals["overtime"] = overtime
	locals["area_input"] = areainput
	locals["area_output"] = mesg

	if len(btexport) > 0 {
		//err := xlsx.SaveAs("./慢sql.xlsx")
		//	if err != nil {
		//		log.Println("excel:", err)
		//}
		//w.Header().Set("Content-Disposition", "attachment; filename=file.xls")
		//http.ServeFile(w, r, "./慢sql.xlsx")

		w.Header().Set("Content-Disposition", "attachment; filename=慢sql.xlsx")
		xlsx.Write(w)
	}
	if len(confsave) > 0 {
		fi, err := os.OpenFile(filepath.Join(dirRun, "itsspider.conf"), os.O_WRONLY|os.O_TRUNC, 0600)
		if err == nil {
			fi.WriteString(areainput)
		}
		defer fi.Close()
	}

	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}
	web.RenderHtml(w, dirRun, sitename, "/intermediate/itsspider", locals)
}

func Dingdingai(dirRun, sitename string, w http.ResponseWriter, r *http.Request) {
	contants := strings.Trim(r.FormValue("contants"), "\r\n\t ")
	url := strings.Trim(r.FormValue("url"), "\r\n\t ")
	users := strings.Trim(r.FormValue("users"), "\r\n\t ")
	//url := "https://oapi.dingtalk.com/robot/send?access_token=6e2e1252f44c36e723dac6b3685f9d758e004022cc7507ea929eda8153f8dc47"
	log.Println("##senddingdingrobet...")
	data := "{"
	data += " \"msgtype\": \"text\","
	data += " \"text\": {"
	data += "     \"content\":\"" + contants + "\""
	data += " },"
	data += " \"at\": {"
	data += "     \"atMobiles\": ["
	data += users
	data += "     ], "
	data += "     \"isAtAll\": false"
	data += " }"
	data += "}"

	bd, e := openapi.SendDingDingExecute(url, data)
	log.Println("bd:", bd)
	var area_output bool
	if e == nil {
		area_output = true
		log.Println("dingdinggroup send successfully:", contants)
	} else {
		log.Println("send ddgroup failed")
	}

	locals := make(map[string]interface{})
	t := r.FormValue("t")
	p := r.FormValue("p")
	if t == "0" {
		locals["t"] = 0
	} else if t == "1" {
		locals["t"] = 1
		locals["p"] = p
	}

	locals["contants"] = contants
	locals["url"] = url
	locals["users"] = users
	locals["area_output"] = area_output
	web.RenderHtml(w, dirRun, sitename, "intermediate/dingdingai", locals)
}
